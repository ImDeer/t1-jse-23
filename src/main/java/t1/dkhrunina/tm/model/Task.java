package t1.dkhrunina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.model.IWBS;
import t1.dkhrunina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @Nullable
    private Date created = new Date();

    @Nullable
    private String description = "";

    @NotNull
    private String name = "";

    @Nullable
    private String projectId;

    @NotNull
    private Status status = Status.NOT_STARTED;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return name + (description == null || description.isEmpty() ? "" : (": " + description));
    }

}