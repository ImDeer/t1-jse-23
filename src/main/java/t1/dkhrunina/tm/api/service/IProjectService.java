package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project create(@NotNull String userId, @Nullable String name);

    @NotNull
    Project create(@NotNull String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateById(@NotNull String userId, @Nullable String id, @Nullable String name,
                       @Nullable String description);

    @NotNull
    Project updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name,
                          @Nullable String description);

}